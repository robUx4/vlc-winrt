﻿namespace VLC_WinRT.Model.FileExplorer
{
    public enum RootFolderType
    {
        Library,
        ExternalDevice,
        Network
    }
}
