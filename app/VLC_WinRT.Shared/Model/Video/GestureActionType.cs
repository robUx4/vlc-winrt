﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VLC_WinRT.Model.Video
{
    public enum GestureActionType
    {
        Null,
        Volume,
        Brightness,
        Seek
    }
}
